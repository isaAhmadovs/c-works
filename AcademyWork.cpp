

// Class Academy (MyStat).


#include <iostream>

#include <string>

#include <vector>

#include <exception>

using namespace std;

enum Spec { Programmers, Designers, SysAdmins };

class Student
{
	int id;
	string name;
	string surname;
	int age;
	string phone;
	double average;
public:
	Student(int id, string name, string surname, int age, string phone, double average)
		: id(id), name(name), surname(surname), age(age), phone(phone), average(average) {}

	string getFullName()
	{
		return name + " " + surname;
	}
	string getFullStr() {
		return name + " " + surname + " " + phone;
	}
	void setName() {
		cout << "Enter name" << endl;
		string a;
		cin >> a;
		this->name = a;
	}
	void setSurname() {
		cout << "Enter surname" << endl;
		string a;
		cin >> a;
		this->surname = a;
	}
	void setAge() {
		cout << "Enter age" << endl;
		int a;
		cin >> a;
		this->age = a;
	}
	void setPhone() {
		cout << "Enter phone" << endl;
		string a;
		cin >> a;
		this->phone = a;
	}
	void setAverage() {
		cout << "Enter average" << endl;
		double a;
		cin >> a;
		this->average = a;
	}

	string getName() const {
		return name;
	}
	string getSurname()const {
		return surname;
	}
	string getPhone()const {
		return phone;
	}
	int getAge()const {
		return age;
	}
	double getAverage() const {
		return average;
	}
	int getID() const {
		return id;
	}
	void setID() {
		int i;
		cin >> i;
		id = i;
	}
};

class Teacher
{
	string name;
	string surname;
	int age;
	string phone;
public:
	Teacher(string name, string surname, int age, string phone)
		:name(name), surname(surname), age(age), phone(phone) {}

	string getFullName()
	{
		return name + " " + surname;
	}
	string getFullStr() {
		return name + " " + surname + " " + phone;
	}
	void setName() {
		cout << "Enter name" << endl;
		string a;
		cin >> a;
		this->name = a;
	}
	void setSurname() {
		cout << "Enter surname" << endl;
		string a;
		cin >> a;
		this->surname = a;
	}
	void setAge() {
		cout << "Enter age" << endl;
		int a;
		cin >> a;
		this->age = a;
	}
	void setPhone() {
		cout << "Enter phone" << endl;
		string a;
		cin >> a;
		this->phone = a;
	}
	string getName() const {
		return name;
	}
	string getSurname()const {
		return surname;
	}
	string getPhone()const {
		return phone;
	}
	int getAge()const {
		return age;
	}
};
class Group
{
	string name;
	Spec spec;
	vector<Student> group;
	vector<Teacher> tgroup;
public:
	Group(string name, Spec spec) : name(name), spec(spec) {}

	string getName() const
	{
		return name;
	}
	string getSpec() {
		if (spec == 0) {
			return "Programmers";
		}
		else if (spec == 1) {
			return "Designers";
		}
		else  if (spec == 2) {
			return "SysAdmins";
		}
	}
	void addTeacher(string name, string surname, int age, string phone) {
		Teacher t(name, surname, age, phone);
		tgroup.push_back(t);
	}
	void addStudent(int id, string name, string surname, int age, string phone, double average)
	{
		Student s(id, name, surname, age, phone, average);
		group.push_back(s);
	}

	int groupSize()
	{
		return group.size();
	}
	int tgroupSize()
	{
		return tgroup.size();
	}

	Student& getStudent(int i)
	{
		return group[i];
	}
	Teacher& getTeacher(int i)
	{
		return tgroup[i];
	}
	void  DelTeacher(int i)
	{
		tgroup.erase(tgroup.begin() + i);
		cout << " Teacher Deleted Succsesfully" << endl;
	}
	void deleteStudent(int i) {
		group.erase(group.begin() + i);
		cout << " Student Deleted Succsesfully" << endl;
	}
	void editStudent() {
		int i;
		cout << "Enter index:  " << endl;
		cin >> i;
		group.erase(group.begin() + i);
		int id, age;
		string name, phone, surname;
		double average;
		cout << "Enter ID: ";
		cin >> id;
		cout << "Enter age: ";
		cin >> age;
		cout << "Enter name: ";
		cin >> name;
		cout << "Enter phone: ";
		cin >> phone;
		cout << "Enter surname: ";
		cin >> surname;
		cout << "Enter average: ";
		cin >> average;
		addStudent(id, name, surname, age, phone, average);
	}
	void editTeacher() {
		int i;
		cout << "Enter index:  " << endl;
		cin >> i;
		tgroup.erase(tgroup.begin() + i);
		int age;
		string name, phone, surname;
		cout << "Enter age: ";
		cin >> age;
		cout << "Enter name: ";
		cin >> name;
		cout << "Enter phone: ";
		cin >> phone;
		cout << "Enter surname: ";
		cin >> surname;
		addTeacher(name, surname, age, phone);
	}
	friend class Program;
};

class Academy
{
	vector<Group> groups;
public:
	vector<Group>& getGroups()
	{
		return groups;
	}

	Group& getGroup(int index)
	{
		return groups[index];
	}

	void addSysAdmingroup(string name)
	{
		Spec spec = SysAdmins;
		Group g(name, spec);
		groups.push_back(g);
	}
	void addDesignersgroup(string name)
	{
		Spec spec = Designers;
		Group g(name, spec);
		groups.push_back(g);
	}
	void addProgrammersgroup(string name)
	{
		Spec spec = Programmers;
		Group g(name, spec);
		groups.push_back(g);
	}

	void deleteGroup(int i)
	{
		groups.erase(groups.begin() + i);
	}
};

class Program
{
	Academy& academy;
public:
	Program(Academy& academy) : academy(academy) {};
	void groupMenu(int c) {
		cout << "                         " << academy.getGroup(c).getName() << endl;
		cout << "1. List of students and teachers" << endl;
		cout << "2. Add student" << endl;
		cout << "3. Edit student" << endl;
		cout << "4. Delete student" << endl;
		cout << "5. Get Full Info" << endl;
		cout << "6. Lay off teacher" << endl;
		cout << "7. Edit teacher" << endl;
		cout << "8. Add teacher" << endl;
		char a;
		cin >> a;
		if (a > '8') {
			exception ex;
			throw ex;
			ex.what();
		}
		if (a == '1') {
			for (int i = 0; i <academy.getGroup(c).group.size(); i++)
			{
				cout << academy.getGroup(c).getStudent(i).getFullName() << endl;

			}
			cout << endl;
			for (int b = 0; b<academy.getGroup(c).tgroup.size(); b++) {
				cout << "Teacher: " << academy.getGroup(c).getTeacher(b).getFullName() << endl;

			}
			cout << endl;
			groupMenu(c);
		}
		else if (a == '2') {
			int id, age;
			string name, phone, surname;
			double average;
			cout << "Enter ID: ";
			cin >> id;
			cout << "Enter age: ";
			cin >> age;
			cout << "Enter name: ";
			cin >> name;
			cout << "Enter phone: ";
			cin >> phone;
			cout << "Enter surname: ";
			cin >> surname;
			cout << "Enter average: ";
			cin >> average;
			academy.getGroup(c).addStudent(id, name, surname, age, phone, average);
			groupMenu(c);
		}

		else if (a == '4') {
			int i;
			cout << "Enter Index ";
			cin >> i;
			academy.getGroup(c).deleteStudent(i);
			groupMenu(c);
		}


		else if (a == '3') {
			academy.getGroup(c).editStudent();
			groupMenu(c);
		}
		else if (a == '5') {

			for (int i = 0; i < academy.getGroup(c).groupSize(); i++)
			{
				cout << academy.getGroup(c).getStudent(i).getFullStr() << endl;
				cout << "Age: " << academy.getGroup(c).getStudent(i).getAge() << endl;
				cout << "Average: " << academy.getGroup(c).getStudent(i).getAverage() << endl;
				cout << "ID: " << academy.getGroup(c).getStudent(i).getID() << endl;

			}
			for (int z = 0; z < academy.getGroup(c).tgroupSize(); z++)
			{
				cout << academy.getGroup(c).getTeacher(z).getFullStr() << endl;
				cout << "Age: " << academy.getGroup(c).getTeacher(z).getAge() << endl;
			}
			groupMenu(c);
		}
		else if (a == '6') {
			int i;
			cout << "Enter Index ";
			cin >> i;
			academy.getGroup(c).DelTeacher(i);
			groupMenu(c);
		}
		else if (a == '7') {
			academy.getGroup(c).editTeacher();
			groupMenu(c);
		}
		else if (a == '8') {
			int id, age;
			string name, phone, surname;
			double average;
			cout << "Enter age: ";
			cin >> age;
			cout << "Enter name: ";
			cin >> name;
			cout << "Enter phone: ";
			cin >> phone;
			cout << "Enter surname: ";
			cin >> surname;
			academy.getGroup(c).addTeacher(name, surname, age, phone);
			groupMenu(c);
		}
	}
	void mainMenu()
	{
		system("cls");
		cout << "1. Menu of groups\n";
		cout << "2. List of Groups\n";
		cout << "3. Add Group\n";
		cout << "4. Delete Group" << endl;
		cout << "5. Get Full info about groups\n";
		char a;
		cin >> a;

		if (a > '5') {
			exception ex;
			throw ex;
			ex.what();
		}
		if (a == '1') {
			for (int i = 0; i < academy.getGroups().size(); i++)
			{
				cout << academy.getGroup(i).getName() << endl;
			}
			int i;
			cout << "Enter a index of a group: ";
			cin >> i;
			system("cls");
			groupMenu(i);
		}
		else if (a == '5') {
			for (int i = 0; i < academy.getGroups().size(); i++)
			{
				cout << academy.getGroup(i).getName()<<" ";
				cout << academy.getGroup(i).getSpec() << endl;
			}
			system("pause");
			mainMenu();
		}
		else if (a == '2') {
			for (int i = 0; i < academy.getGroups().size(); i++)
			{
				cout << academy.getGroup(i).getName() << "  ";
				cout << academy.getGroup(i).getSpec() << endl;
			}
			system("pause");
			system("cls");
			mainMenu();
		}
		else if (a == '3') {

			string name;
			cout << "Enter Name: ";
			cin >> name;
			cout << "Default specialization  is programmers do you want to change it? "<<endl;
			cout << "1)Yes,to SysAdmins" << endl;
			cout << "2)Yes,to Designers" << endl;
			cout << "3)No,leave Programmers" << endl;
			char n;
			cin >> n;
			if (n ==  '3') {
				academy.addProgrammersgroup(name);
			}
			else if (n == '2') {
				academy.addDesignersgroup(name);
			}
			else if (n == '1') {
				academy.addSysAdmingroup(name);
			}
			else{
				cout << "Wrong number" << endl;
				system("pause");
				mainMenu();
			}
			mainMenu();
		}
		else if (a == '4') {
			int i;
			cout << "Enter Index";
			cin >> i;
			academy.deleteGroup(i);
			mainMenu();
		}
		cout << endl;
		system("pause");

		vector<Group> tmp = academy.getGroups();
		for (int i = 0; i < tmp.size(); i++)
		{
			cout << tmp[i].getName() << endl;
		}
	}
};

int main()
{
	Academy acad;
	acad.addProgrammersgroup("PVSDM_1712_RU");
	acad.addProgrammersgroup("PSSDA_1711_RU");

	acad.getGroup(0).addStudent(1, "Isa", "Ahmadov", 14, "+994-50-304-33-32", 10.0);
	acad.getGroup(0).addStudent(2, "Tubu", "Ibrahimova", 100, "+994-77-458-05-71", 10.0);
	acad.getGroup(0).addStudent(3, "Yunis", "Ibadullaev", 100, "+994-50-255-70-17", 10.0);


	acad.getGroup(1).addStudent(1, "Aflettdun", "Seyfullayev", 100, "+???", 10.0);
	acad.getGroup(1).addStudent(2, "Ruslan", "Aslanov", 100, "+???", 10.0);
	acad.getGroup(1).addStudent(3, "Emil", "Kerimov", 100, "+???", 10.0);

	acad.getGroup(1).addTeacher("Alladin", "Mellim", 49, "+99395486478932");

	acad.getGroup(0).addTeacher("Gleb", "Skripnikov", 100, "+994-51-604-99-48");
	acad.getGroup(0).addTeacher("Rovshan", "Agayev", 100, "+994-055-405-54-04");

	Program program(acad);
	program.mainMenu();


	return  0;
}